## Antes de empezar

Para utilizar este proyecto es necesario ejecutar los siguientes comandos.

Nota: Este proyecto utiliza PostgreSQL

```bash
# Creamos el entorno virtual
virtualenv -p python3 venv

# Activamos el entorno virtual
source venv/bin/activate

# Instalamos los requerimientos
pip install -r requirements.txt

# Hacemos las migraciones
cd get_and_post/
python manage.py migrate

# Corremos el servidor
python manage.py runserver
```
