from django.urls import path
from . import views

app_name = 'employees'
urlpatterns = [
  path('', views.get_employees, name='get_employees'),
  path('create/', views.create_employee, name='create_employee'),
  path('get_post_employee/', views.get_post_employee, name='get_post_employee'),
]