from django.db import models

class Employee(models.Model):
  last_name = models.CharField(max_length=250)
  first_name = models.CharField(max_length=250)
  salary = models.DecimalField(max_digits=8, decimal_places=2)
  timestamp = models.DateTimeField(auto_now_add=True)
  updated = models.DateTimeField(auto_now=True)

  def __str__(self):
    return f'{self.last_name} {self.first_name}'