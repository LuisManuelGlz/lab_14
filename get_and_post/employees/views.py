from django.shortcuts import render, redirect
from .models import Employee
from .forms import EmployeeForm

def get_employees(request):
  context = {
    'employees': Employee.objects.all()
  }
  return render(request, 'employees/employee_list.html', context)

def create_employee(request):
  return render(request, 'employees/employee_form.html')

def get_post_employee(request):
  # si es método POST
  if request.method == 'POST':
    form = EmployeeForm(request.POST)
    if form.is_valid():
      form.save()
      print('Se utilizó el método POST')
      return redirect('employees:get_employees')
  # si es método GET
  elif request.method == 'GET':
    form = EmployeeForm(request.GET)
    if form.is_valid():
      form.save()
      print('Se utilizó el método GET')
      return redirect('employees:get_employees')